# OSCAR Group

The Oscar Hernández Seminumerical Cosmology/Astrophysics Research Group is working on a collection of related projects based at McGill under the supervision of Oscar Hernández.

## Documentation

The logs and presentations of the contributors can be found in [doc/logs](https://gitlab.com/hanshopkins/oscar-group/-/tree/main/doc/logs) and [doc/presentations](https://gitlab.com/hanshopkins/oscar-group/-/tree/main/doc/presentations).

 To inform yourself on how we use RBFI with ARES check out [src/rbfi](https://gitlab.com/hanshopkins/oscar-group/-/tree/main/src/rbfi).

## Getting Started

First ensure that [ARES](https://github.com/mirochaj/ares) and [21cmFAST](https://github.com/21cmfast/21cmFAST) are installed as they are required packages for using this repository (Currently only ARES is required). 

Note that these programs are designed to work on Unix-based systems so if you are using windows you may want to install [WSL](https://docs.microsoft.com/en-us/windows/wsl/install) and install these programs there. 

Once you are ready, clone a copy and of this repository in your home directory:

    cd ~
    git clone https://gitlab.com/hanshopkins/oscar-group.git



