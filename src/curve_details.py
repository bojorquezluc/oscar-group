import numpy as np
from numba import njit, prange
redshifts = np.linspace(5,30,40)

#For an example, run this function with /data/example_data/fstar_fx_example_curves.gz loaded into a numpy array. Every row is the values of a curve at redshifts = np.linspace(5,30,40). The corresponding values for the parameters can be found in /data/example_data/fstar_fx_list.gz

#@njit(parallel=True)
def curve_details (curves):
	output_array = np.empty([curves.shape[0], 3], dtype=np.float64) #this holds the depth, position of the trough, and the width of the trough

	#finding the minumim index in the array
	min_idx = np.argmin(curves, axis = 1)
	
	#now we can use the parabola to find the details about the curve
	for i in prange(curves.shape[0]):
		#here we're approximating the bottom of the curve by a parabola that goes through the nearest three points to the bottom
		A = np.empty([3,3], dtype=np.float64)
		for ii in range(3):
			x_ii = redshifts[min_idx[i]-1+ii]
			A[ii][0] = 1; A[ii][1] = x_ii; A[ii][2] = x_ii*x_ii
		d = np.asarray([curves[i][min_idx[i]-1], curves[i][min_idx[i]], curves[i][min_idx[i]+1]])
		c,b,a = (np.linalg.inv(A)@d).tolist() #getting the parabola coefficients

		output_array[i][0] = b**2/(4*a) - b**2/(2*a) + c #formula for the distance between 0 and the bottom of the parabola
		output_array[i][1] = -b/(2*a) #formula for the position of the bottom of the parabola
	
		#next finding the halfway width
		half_height = output_array[i][0]/2
		#searching for the position of the half height by a linear search. I don't think it can be faster than this.
		half_idx_l = min_idx[i] #half_idx is our searching variable for the index where the curve passes above half_height on the left side
		while (curves[i][half_idx_l] < half_height):
			if half_idx_l == 0:
				raise IndexError("Halfway up the trough is outside the range of the curve " + str(i) + ". Increase redshift range and retry.")
			half_idx_l -= 1
		#we approximate the exact position of halfheight using a linear interpolation
		A = np.asarray([[1,redshifts[half_idx_l]],[1, redshifts[half_idx_l+1]]])
		d = np.asarray([curves[i][half_idx_l], curves[i][half_idx_l+1]])
		b,a = (np.linalg.inv(A)@d).tolist()
		half_height_l_z = (half_height-b)/a
	
		#we need to redo this computation for the right side
		half_idx_r = min_idx[i] #half_idx is our searching variable for the index where the curve passes above half_height on the right side
		while (curves[i][half_idx_r] < half_height):
			if half_idx_r == 0:
				raise IndexError("Halfway up the trough is outside the range of the curve " + str(i) + ". Increase redshift range and retry.")
			half_idx_r += 1
		#we approximate the exact position of halfheight using a linear interpolation
		A = np.asarray([[1,redshifts[half_idx_r-1]],[1, redshifts[half_idx_r]]])
		d = np.asarray([curves[i][half_idx_r-1], curves[i][half_idx_r]])
		b,a = (np.linalg.inv(A)@d).tolist()
		half_height_r_z = (half_height-b)/a

		output_array[i][2] = half_height_r_z - half_height_l_z # the width is the difference

	return output_array
