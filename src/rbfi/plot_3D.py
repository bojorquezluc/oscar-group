import numpy as np
import argparse
import functions as fn


def main():
    # DEFAULT ARGS:
    model = "fX-100-100-0.1-0.5-gaussian-1-all-rbfi.pickle"
    show_plot = True

    # Checks for user specified arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--model")
    parser.add_argument("-s", "--show_plot", type = bool)
    args = parser.parse_args()

    if (args.model != None):
        model = args.model  
    if (args.show_plot == "False" ) or (args.show_plot == "false" ) or (args.show_plot == "F" ) or (args.show_plot == "f" ):
        show_plot = False
    
    fn.plot_param_space(model, show_plot)


if __name__ == "__main__":
    main()