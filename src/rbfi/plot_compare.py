import argparse
import functions as fn


def main():
    # DEFAULT ARGS:
    model = "fX-100-100-0.1-0.5-gaussian-1-all-rbfi.pickle"
    param_val = 0.2
    show_plot = True

    # Checks for user specified arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--model")
    parser.add_argument("-v", "--param_val", type = float)
    parser.add_argument("-s", "--show_plot")
    parser.add_argument("-t", "--start", type = float)
    parser.add_argument("-T", "--stop", type = float)
    args = parser.parse_args()

    if (args.model != None):
        model = args.model
    if (args.param_val != None):
        param_val = args.param_val
    if (args.start != None):
        start = args.start
    if (args.stop != None):
        stop = args.stop
    if (args.show_plot == "False" ) or (args.show_plot == "false" ) or (args.show_plot == "F" ) or (args.show_plot == "f" ):
        show_plot = False

    # Function that returns the dTb/redshift curve from ares
    def func(x, a, param = "fX"):
        dict = {param: a}
        curve = fn.call_ares(dict, x)
        return curve

    fn.plot_compare(model, func, param_val, show_plot, start, stop)

if __name__ == "__main__":
    main()
